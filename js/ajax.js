
	Array.prototype.forEach.call( document.getElementsByTagName('BUTTON'),  function(btn){
		var role = btn.getAttribute('data-role');
		btn.addEventListener('click', handler);
	});
	
	function authoriseMe(authForm, profileForm) {
		var inputEmail 	= document.forms[0].elements['email'].value;
		var inputPass 	= document.forms[0].elements['password'].value;
		var formMethod 	= document.forms[0].attributes.method.value;
		var formAction 	= document.forms[0].attributes.action.value;
		var formEnctype = document.forms[0].attributes.enctype.value;
		var err = document.querySelectorAll('form > div[data-role="json-request-error"]');
			err[0].innerHTML = null;
		var preload = document.querySelectorAll('form > span[data-role="json-request-preloader"]');
		var userData;
		var body = 'email=' + inputEmail + '&password=' + inputPass;
		var xhr = new XMLHttpRequest();
		
		xhr.open(formMethod, formAction, true);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhr.addEventListener('load', function () {
        		if (xhr.status >= 200 && xhr.status <= 299) {
					var userData = JSON.parse(xhr.responseText);
					authForm.classList.remove("open");
					authForm.classList.toggle("lock");
					profileForm[0].classList.toggle("lock");
					
					Array.prototype.forEach.call(profileForm[0].children, function(userTag){
						var userField = userTag.getAttribute('data-role');
						switch (userField){
							case 'json-request-user-profile-avatar':
							userTag.setAttribute('src', userData.userpic);
							userTag.setAttribute('alt', userData.name+' '+userData.lastname);
							break;
							case 'json-request-user-profile-fullname':
							var username = document.createElement('b');
								username.innerHTML = userData.name+' '+userData.lastname;
								userTag.appendChild(username);
							break;
							case 'json-request-user-profile-country':
							var country = document.createElement('p');
								country.innerHTML = 'country: '+userData.country;
								userTag.appendChild(country);
							break;
							case 'json-request-user-profile-hobbies':
							var hobbies = document.createElement('i');
								hobbies.innerHTML = 'Hobby:<br/>' + userData.hobbies.join(', ');
								userTag.appendChild(hobbies);
							break;
						}
					});
					document.forms[0].reset();
					//return userData;
				}
				else {
					err[0].innerHTML = '<p>NetworkError: '+xhr.status+'</p>';
				}
			});
			xhr.addEventListener('loadstart', function () {
				authForm.style.opacity = "0.3";
				preload[0].style.visibility = "visible";
				preload[0].innerHTML = '<img src="css/load.gif">';
			});
			xhr.addEventListener('loadend', function () {
				preload[0].style.visibility = "hidden";
				authForm.style.opacity = "1";
			});
			xhr.addEventListener('error', function (event) {
				err[0].innerHTML = '<p>Error status: '+event.target.status+'</p>';
				console.warning(event);
			});
		xhr.send(body);
		return xhr;
	}
	
	function handler(event){
		event.preventDefault();
		var whatBtnRole = event.target.attributes['data-role'].value; //console.log(event.target.attributes['data-role'].value);
		var authForm = document.querySelectorAll('form[data-role="json-request-form"]');
		var profileForm = document.querySelectorAll('div[data-role="json-request-user-profile"]');
		switch (whatBtnRole){
				case 'json-request-form-submit': /*check login*/
					authoriseMe(authForm[0], profileForm);
				break;
				case 'json-request-user-profile-logout': /*verify out*/
					authForm[0].classList.remove("lock");
					authForm[0].classList.toggle("open");
					profileForm[0].classList.toggle("lock");
					var clearAvatar = profileForm[0].getElementsByTagName('img');
						clearAvatar[0].setAttribute('src', '');
						clearAvatar[0].setAttribute('alt', '');
					var clearNodes = profileForm[0].getElementsByTagName('span');
						console.log(clearNodes);
						Array.prototype.forEach.call(clearNodes, function(node){
							while (node.lastChild) {
								node.removeChild(node.lastChild);
							}
						});
				break;
		}
	}

	